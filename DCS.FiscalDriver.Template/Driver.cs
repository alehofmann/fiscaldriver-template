﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;



namespace DCS.FiscalDriver.Template
{
    public class Driver		
    {
	    private int _lastError;
	    private string _lastErrorString;
		public Driver()
		{			
			
		}

	    public bool Create(string drive, string dummy = null)
	    {
			/* Chequear existencia, leer y verificar validez de variable de entorno NW
			 * Inicializar loggeo
			 * Hacer lo mismo con los INIs y otros parametros de configuracion de este driver.
			 * Ejemplos serían distintos porcentajes de TAX, descuentos especiales para Jubilados o discapacitados, configuracion del hardware de impresora (comport)
			 * Inicializar hardware (impresora) y verificar que esté online
			 * Loggear detalladamente todo lo que se hace
			*/

			return true;
			// o return false en caso de fallar algo de toda la incializacion

	    }

	    public bool DriverPrintsFromTransactionObject => true; //Devuelve true porque esta impresora es ADEMAS un controlador fiscal

			
		public int LastError => _lastError;
	    public string LastErrorString => _lastErrorString;

	    public bool NeedsZClose()
	    {
			//A pesar del nombre, esta rutina lo que hace es devolver 'True' si la transacción puede continuar o 'False' si no puede
			//En un principio la razon por la que no podía proseguir la transacción era porque hacía falta hacer un cierre Z por haber cambiado el día fiscal
			//Pero pueden existir otras razones, como un certificado/habilitación vencida/expirada.

			return true;
	    }

	    public bool PerformZClose(ref int zCloseNumber, bool dummyPrintHeaderAndFooter = true)
	    {
			//Como su nombre lo indica efectúa un cierre 'Z'
			return true;
	    }

	    public bool PerformCashierClose(ref int dummyCashierCloseNumber)
	    {
			//No tengo idea de lo que hace, marce pls aclare
			return true;
	    }

	    public bool PerformXClose()
	    {
			//Cierre 'X'
			return true;
	    }

	    public bool PrintNonFiscalDoc(Collection textLines, ref int docNumber, bool dummyPrintHeader = true,
		    bool dummyCutPaper = true)
	    {
			//Imprime un documento no-fiscal. Simplemente debe imprimir linea x linea lo que viene en la coleccion (de visual basic 6) textLines.
		    //Yo antes que nada convertiría textLines a un tipo de dato nativo de c#
		    foreach (string line in textLines)
		    {
			    
		    }

			return true;
		}

#if DEBUG
	    public bool PrintTransaction(DCSPOSUtility.clsPOSXaction transaction, ref int invoiceNumber,
		    bool dummyCutPaper = true)
	    {
		    bool retVal;
		    DCSPOSUtility.clsPOSCard card;
		    DCSPOSUtility.clsPOSLineItem lineItem;
		    DCSPOSUtility.clsPOSPayment posPayment;
		    string richText;
	    
#else
		 public bool PrintTransaction(DCSPOSUtility.clsPOSXaction transaction, ref int invoiceNumber,
			bool dummyCutPaper = true)
		{
			bool retVal;
			object card;
			object lineItem;
			object posPayment;
			string richText;
#endif			
			return true;


			/* Esto se encarga de la impresion fiscal en sí.
			 * La compilación condicional es para tener InteliSense cuando se programa pero que las declaraciones sean 'as object' para el modulo compilado.
			 * De esta forma para versiones nuevas del driver puede ir extendiendose la definicion de estas clases sin volverlo incompatible con versiones mas antiguas del POS o Kiosk que lo esté invocando
			 * el parametro 'transaction' es la estructura con todo lo que se necesita para hacer la impresion fiscal.
			 */
		}

	    public bool OpenTransaction(Collection fiscalParameters)
	    {
			/* Aca abre la transaccion, lo cual segun el driver puede incluir agregar algun record a una tabla
			 * o (como en el caso de Filipinas) dar a elegir mediante un form el tipo de factura (normal, jubilado, discapacitado, peza).
			 * En base al tipo de factura seleccionada hay que setear TaxExempt y DiscountId. El DiscountId para cada tipo de facturacion debera ser leido de algun INI en el Create
			 * Si por algun motivo no se puede abrir la transaccion, ya sea porque 'NeedsZClose', la impresora no responde, no tiene papel, o simplemente en el form en pantalla el cajero seleccionó 'cancel'
			 * OpenTransaction devolverá FALSE. Si está todo bien devuelve TRUE
			 * fiscalParameters podrá tener mas datos en versiones nuevas de drivers. Yo comentado pongo lo que veo hasta hoy segun necesidades filipinas
			 */

		    bool isTaxExempt;
		    int discountId;

			isTaxExempt = true;
		    discountId = 1;

		    fiscalParameters.Add(isTaxExempt, "TaxExempt");
		    fiscalParameters.Add(discountId, "DiscountId");

			//If not NeedsZClose and .....
			return true;
			//else
			//return false;
	    }

	}
}

